# EAUM Ontology

The ontology is mainly created with the [Protégé editor](https://protege.stanford.edu/)
of the Stanford University.

## Overview

Based on a map usage task that is either static (search, query, pattern detection) or
dynamic (temporal change detection, thematic and geometric change detection) in nature,
requirements arise for the design of the EAUM. The requirements are formulated in terms
of dimensional numbers and essentially relate to individual EAUM properties of the EAUM
to be generated (outer shape (global, geometric), area size (local, geometric),
neighborhood relations (local, topological)) that have a positive impact on the solution
of a particular task. There is a relationship between dimension number and one or more
of the three criteria. Measure numbers are computed from a loaded data set and, along
with the task, are the most important basis for the decision-making process. Which
measure numbers are in favor of a particular EAUM in a particular task context has yet
to be determined through user studies. It should also be found out whether a particular
EAUM form (SimpleGrid vs. GrowingGrid vs. flow-based, see
[pyEAC](https://gitlab.com/g2lab/pyeac) and
[go_cart](https://github.com/Flow-Based-Cartograms/go_cart)) is more suitable for a task
than another. The result should be included in the recommendation for action together
with the measurements.

## Architecture

The general architecture is depicted in the following image:

<div style="text-align: center;">
    <img src="eaum-ontology.png" alt="EAUM-Ontology relations" width="800px">
</div>

The workflow is thought to be as follows: A task has a specific requirement that is
fulfilled by an EAUM which is therefor usable for the task.

### OWL classes

The circles in the above image are the main OWL classes *Task*, *Requirement* and
*EAUM*. EAUM is further split up into *SimpleGrid* and *GrowingGrid* (according to
cartogram generation methods of the g2lab python tool
[pyEAC](https://gitlab.com/g2lab/pyeac)). Until now there are two subclasses for
*Requirement*: *Shape*, which stands for putting priority on the  outer shape and
*Neighborhood*, which stands for putting weight on keeping the original neighborhood
relationships of the input dataset. Likewise *Area* stands for emphasizing certain
spatial entities, whose area size should not exceed a certain threshold. A *Task* is
either a *StaticTask* or a *DynamicTask*.

### OWL object properties

The arrows in the image are OWL object properties that are attached to the classes they
originate from with a range for the classes they point to.

### OWL data properties

OWL data properties created so far are:

- EAUM
  - **neighborhoodParameter** - A numerical description of the actual parameter that
    describes and controls the neighborhood weight
  - **shapeParameter** - As above but for priotizing shape
  - **areaSizeParameter** - Essentially the same as the areaSizeValue for the Area
    requirement class
  - **areaSizeIDParameter** - Essentially the same as the areaSizeIDValue for the Area
    requirement class

- Neighborhood
  - **neighborhoodValue** - A numerical description of the requirement value for
    prioritizing neighborhood. Holds also information about the allowed numerical range.
- Shape
  - **shapeValue** - As above but for priotizing shape. Holds also information about the
    allowed numerical range
- Area
  - **areaSizeValue** - A list of area thresholds
    for certain spatial entities. Needs to be in line with areaSizeIDValue
  - **areaSizeIDValue** - A list of IDs of spatial entities whose area size should not
    exceed a certain threshold. Positions in this list should correspond to the area
    size value in the areaSizeValue property

These data properties are very basic and exemplary. They will be rearranged, adapted or
removed once they are formulated more clearly in future research an when user studies
bring further insights. So far they should make queries on the ontology possible.

## General workflow description

At this stage, two scenarios are conceivable for the use of the ontology:

1. New findings from user studies need to be incorporated into the ontology.
2. An end user with an input dataset and a task makes a request to the ontology and
   receives the name of the EAUM and the associated parameters (shapeParameter,
   neighborhoodParameter) for this task.

### Scenario 1

This scenario solidifies knowledge in the ontology. Here it should be determined, which
requirements are valid for the individual tasks. Specifically, this means forming
instances of the *Requirement* class and setting appropriate minimum values (shapeValue,
neighborhoodValue, ...). These instances must then be linked to the *Task* instances via
the `:hasRequirement` attribute. Consider creating a collection of *EAUM*s with
different values for shapeParameter and neighborhoodParameter at the beginning of the
user studies. As indicated in the next section, new links between Task, Requirement
and EAUM instances can be established using SPIN rules.

### Scenario 2

Precondition for scenario 1 is that there is a collection of *EAUM* instances with fixed
values for the shapeParameter, neighborhoodParameter (tba).  Furthermore, it is
necessary that the relations `EAUM usableFor Task` and `EAUM fulfills Requirement` are
set for the individual *EAUM*s.

This can be realized by formulating SPIN rules for the task class. Using a SPIN engine
(e.g. in RDF4J) corresponding inferences can be formed (see CONSTRUCT query in the next
section). Alternatives to automatic inferencing are to set the EAUM properties manually
(usableFor, fulfills) or to execute SPARQL queries separately (see CONSTRUCT query below
above without `?this`) and then set the properties manually.

## Queries & Rules

One of the goals as depicted in the README is to facilitate decision for an EAUM with
regard to a specific task. It would make sense to make the ontology queryable by an
application that generates EAUMs. Furthermore rules for querying need to be established
since OWL by itself is not intended for this. During research it has been found that
SPARQL and SPIN are suitable for these use cases.

### SPARQL & SPIN

SPARQL, or more specific SPARQL-endpoints, can be used to query a saved ontology via a
REST-API. There are some open source solutions worth trying like [RDF4J](https://rdf4j.org/) or Open Web
Ontobud that provide the possibility to set up a user interface for management and an
endpoint for queries.

SPIN stands for *SPARQL Inference Notation*. With this notation it is possible to define
specific inference rules that are not possible with plain OWL. These rules need to be
attached to specific OWL classes. A specific SPIN-Engine needs to interpret these rules
and build inferences from the results. With [RDF4J](https://rdf4j.org/) it is possible to store the ontology
and also run SPIN rules. Another possibility for fast checking inferences is to use the
[TopBraid Composer](https://www.topquadrant.com/project/introducing_topbraid_composer/),
which is also capable of running SPIN rules.

#### RDF4J

Setting up RDF4J is very easy if one uses
[docker](https://docs.docker.com/engine/install/) with
[docker-compose](https://docs.docker.com/compose/install/). There is a
prepared `docker-compose.yml` that spins up [RDF4J](https://rdf4j.org/) via the command `docker-compose up
-d`. Once the container is running the workbench for managing repositories (which hold
ontologies) is reachable via

`http://localhost:8080/rdf4j-workbench`.

The respective
server or REST-API is reachable via

`http://localhost:8080/rdf4j-server/repositories/{repositoryName}`

For using SPIN the repository needs to be of type `Native Store + RDFS and SPIN
support`. Via the menu point `Add` one can upload for example the
[eaum.ttl](ontology/eaum.ttl) ontology file. A SPARQL-Query can then be run via the
`Queries` menu point. Rules are executed when the ontology is loaded into a created
repository.

### Task rule

The rule for connecting Tasks with EAUMs and Requirement instances is attached to the
Task class and depicted in the following:


```rdf
###  http://www.g2lab.net/ont/eaum#Task
:Task rdf:type owl:Class ;
    spin:rule [ rdf:type sp:Construct ;
                sp:text """
                        PREFIX : <http://www.g2lab.net/ont/eaum#>
                        CONSTRUCT {
                            ?eaum :usableFor ?this .
                            ?eaum :fulfills ?nbReq .
                            ?eaum :fulfills ?shpReq .
                        } WHERE {
                            ?eaum :shapeParameter ?eaumShpParam .
                            ?eaum :neighborhoodParameter ?eaumNbParam .
                            {
                                SELECT (?shpValue AS ?minShpValue) (?req AS ?shpReq) ?this WHERE {
                                    ?this :hasRequirement ?req .
                                    ?req :shapeValue ?shpValue .
                                }
                            }
                            {
                                SELECT (?nbValue AS ?minNbValue) (?req AS ?nbReq) WHERE {
                                    ?this :hasRequirement ?req .
                                    ?req :neighborhoodValue ?nbValue .
                                }
                            }
                            FILTER ( ?eaumShpParam >= ?minShpValue && ?eaumNbParam >= ?minNbValue ) .
                        }"""
            ] ;
    rdfs:comment "A cartographic task" ;
    rdfs:label "Task" .
```

Essentially it says, for all EAUM instances check if the respective EAUM parameters
fulfill the minimum requirements of the current Task instance. Then set the properties
`:usableFor` and `:fufills` of the matching EAUM with the name of the current Task
instance and its Requirement instances.

### Enduser query

An exemplary query after setting up the SPARQL-endpoint and running the SPIN rules could
then be:

```sparql
PREFIX : <http://www.g2lab.net/ont/eaum#>
SELECT DISTINCT ?eaum ?shpParam ?nbParam ?task WHERE {
  ?eaum :usableFor ?task .
  FILTER (?task in (:PatternDetection)) .
  ?eaum :shapeParameter ?shpParam .
  ?eaum :neighborhoodParameter ?nbParam .
}
```

This query will return the EAUMs that are usable for the task PatternDetection together
with its parameters. An end user could then either use these parameters as an
orientation or select the respective EAUM from the built up library.


## Difficulties and outlook

### Difficulties

The main difficulties are related to setting up the SPIN rules as well as using up to
date technologies.

#### OWL is not a language for rule based inference

OWL was not created with the intention to validate data but rather for classification,
i.e. that a specific object is a certain type of object. Therefore [SPIN was
created](https://spinrdf.org/spin-shacl.html) by
[TopQuadrant](https://www.topquadrant.com/) who also created the TopBraid Composer
mentioned above. With SPIN it is possible to attach inference or data validation rules
to specific classes, which are expressed as SPARQL queries. SPARQL is the de-facto query
language for working with RDF and linked data in the web.

#### Lack of software for working with OWL and SPIN together

The main problem is that an additional SPIN-engine is needed for interpreting the rules.
While the TopBraid Composer supports SPIN rules out of the box (but apart from the no
longer supported free edition, is proprietary software), the difficulties are to find
implementationa for working with OWL and SPIN together. The popular Protégé editor for
example doesn't support SPIN but SWRL (Semantic  Web  Rule Language) for rule based
inference. But SWRL did not manage to get broad adoption in the industry and never
became a W3C standard in contrast to SPIN, resp. SPARQL.
[SpinRDF](https://github.com/spinrdf/spinrdf) has been made open source by the initial
developers but is an extension to [Apache Jena ](https://jena.apache.org/), which is
another framework for working with RDF and linked data and not trivial to set up
correctly.

#### Working solutions with limited support for SPIN

The only working solution for OWL and SPIN is at the moment using Protégé and RDF4J. In
detail this means developing the base ontology classes with the Protégé editor,
executing the SPIN rules when uploading the ontology into an RDF4J repository and query
RDF-triples via the SPARQL-endpoint. However, the developers of RDF4J mention that their
implementation of SPINrdf is not longer actively maintained and also has problems when
data in the repository is updated, which then can break inferecing rules leading to
inconsitency ([reference](https://rdf4j.org/documentation/programming/spin/)). Another
problem is when an existing ontology is update with data upload. RDF4J does not clean
dublicates making it sometimes necessary to clear all OWL statements in the repository
and re-upload the changed ontology.

### Outlook

## Rule based inference

Regarding rule based inference [SHACL](https://www.w3.org/TR/shacl/) is worth
a try. SHACL stands for **SHA**pes **C**onstraint **L**anguage, whereby "shape" means
the description of the shape of an object/instance of an OWL class in terms of
constraints or rules about its properties. It was made a W3C standard in 2017 and is
supported in many implemenatations that are working with OWL (Protégé, Apache Jena and
RDF4J are supporting it). Therefore it is thought to be more future proof than SPIN
rules.

## Linked data framework

While rapid development and prototyping is possible with RDF4J it is not clear how it
performs in a production environment. However, for this small project it should be
sufficient as the database up to now is relatively small. Only issues so far are the
sometimes cumbersome user interface for managing repositories, uploading ontologies and
querying. The above mentioned Apache Jena framework is worth a try together with SHACL.
An alternative UI for RDF4J is supplied by [Open Web
Ontobud](https://github.com/Tibblue/Open-Web-Ontobud).

## References

### Web references

- RDF
  - [Wikipedia](https://en.wikipedia.org/wiki/Resource_Description_Framework)
  - [RDF primer](https://www.w3.org/TR/rdf11-primer/)
- Semantic Web & Linked data
  - OpenHPI Tutorials
    - [Linked Data Engineering (Semantic Web)](https://www.youtube.com/playlist?list=PLoOmvuyo5UAfY6jb46jCpMoqb-dbVewxg)
      - [First Steps in OWL](https://www.youtube.com/watch?v=BbcD-ah8dtE&list=PLoOmvuyo5UAfY6jb46jCpMoqb-dbVewxg&index=19)
    - [Knowledge Engineering with Semantic Web Technologies](https://www.youtube.com/playlist?list=PLoOmvuyo5UAcBXlhTti7kzetSsi1PpJGR)
      - [Web Ontology Language OWL](https://www.youtube.com/watch?v=x7GtYNEWIKE&list=PLoOmvuyo5UAcBXlhTti7kzetSsi1PpJGR&index=38)
    - [Semantic Web Technologies](https://www.youtube.com/playlist?list=PLoOmvuyo5UAeihlKcWpzVzB51rr014TwD)
      - [Web Ontology Language OWL](https://www.youtube.com/watch?v=IgMB71mG42I&list=PLoOmvuyo5UAeihlKcWpzVzB51rr014TwD&index=34)
  - [Introducing Linked Data And The Semantic Web](http://www.linkeddatatools.com/semantic-web-basics)
  - [What Are Classes And Individuals in OWL?](http://www.linkeddatatools.com/help/classes)
- Architecture
  - [Ontology101](https://protege.stanford.edu/publications/ontology_development/ontology101.pdf)
  - [How to Design your own Ontology](https://www.youtube.com/watch?v=pabULZ_eQp4&list=PLoOmvuyo5UAfY6jb46jCpMoqb-dbVewxg&index=23)
  - [Ontology Design](https://www.youtube.com/watch?v=Hg7lXd6Mngo&list=PLoOmvuyo5UAeihlKcWpzVzB51rr014TwD&index=41)
- Endpoints, queries and rules
  - RDF4J
    - [The Eclipse RDF4J Framework](https://rdf4j.org/about/)
    - [Getting Started With RDF4J](https://rdf4j.org/documentation/tutorials/getting-started/)
    - [RDF4J Server and Workbench](https://rdf4j.org/documentation/tools/server-workbench/)
      - Docker container for RDF4J that provides both: https://hub.docker.com/r/eclipse/rdf4j-workbench
  - SPARQL
    - [Wikipedia](https://en.wikipedia.org/wiki/SPARQL)
    - [How to query RDFS SPARQL](https://www.youtube.com/watch?v=fh11N7vwnRA&list=PLoOmvuyo5UAcBXlhTti7kzetSsi1PpJGR&index=14)
    - [SPARQL 1.1 Overview](https://www.w3.org/TR/sparql11-overview/)
  - SPIN
    - [Getting Started with SPARQL Rules (SPIN)](https://www.topquadrant.com/spin/tutorial/)
    - [Why SPIN](https://www.topquadrant.com/technology/sparql-rules-spin/)
    - [SPIN - SPARQL Inference Notation](https://spinrdf.org/)
  - SHACL
    - [Wikipedia](https://en.wikipedia.org/wiki/SHACL)
    - [From SPIN to SHACL](https://spinrdf.org/spin-shacl.html)
    - [SHACL and OWL compared](https://spinrdf.org/shacl-and-owl.html) (contains also a historical perspective on SPIN)
    - [One Ontology, One Data Set, Multiple Shapes with SHACL](https://www.youtube.com/watch?v=apG5K3zc4V0) Tara Raafat

### Scientific literature

- [Iosifescu-Enescu & Hurni, 2007 - Towards cartographic ontologies or "how computers learn cartography"](https://icaci.org/files/documents/ICC_proceedings/ICC2007/documents/doc/THEME%201/oral%205/1.5.3%20TOWARDS%20CARTOGRAPHIC%20ONTOLOGIES%20OR%20%E2%80%9CHOW%20COMPUTERS%20LEAR.doc)
- [Smith, 2010 - Designing a cartographic ontology for use with expert systems](https://www.isprs.org/proceedings/xxxviii/part4/files/Smith.pdf)
- [Penaz et al., 2014 - Design and construction of knowledge ontology for thematic cartography domain](https://www.researchgate.net/publication/259850543_Design_and_construction_of_knowledge_ontology_for_thematic_cartography_domain)
- [Bassiliades, 2018 - SWRL2SPIN: A tool for transforming SWRL rule bases in OWL ontologies to object-oriented SPIN rules](https://arxiv.org/abs/1801.09061)
  - Supplementary Material:
    - https://github.com/nbassili/SWRL2SPIN
    - http://users.auth.gr/nbassili/swrl2spin/
- [Varanka & Usery, 2018 - The map as knowledge base](https://doi.org/10.1080/23729333.2017.1421004)
- [Huang, 2019 - Towards knowledge-based geovisualisation using Semantic Web technologies: a knowledge representation approach coupling ontologies and rules](https://doi.org/10.1080/17538947.2019.1604835)
  - Supplementary Material:
    - https://github.com/RightBank/Knowledge-based-geovisualisation
    - https://github.com/RightBank/Knowledge-based-integration-and-visualisation
- [Pareti et al., 2019 - SHACL Constraints with Inference Rules](https://arxiv.org/abs/1911.00598)
- [Ding et al., 2020 - A Framework Uniting Ontology-Based Geodata Integration and Geovisual Analytics](https://www.mdpi.com/2220-9964/9/8/474)
  - Supplementary material:
    - https://github.com/dinglinfang/suedTirolOpenDataOBDA/
