# Ontology for task-oriented usage of Equal-Area-Unit-Maps (EAUM)

This ontology is primarily intended to represent the decision process by which an EAUM
with specific parameters is selected based on a map usage task and resulting requirements
(including metrics determined by means of user evaluations). The main goal in the
broader sense is to simplify the decision-making process for inexperienced users. On the
other hand, the knowledge gained through research and user studies is recorded in a
generally valid and implementation-independent form.

Measures and parameters so far describe e.g. whether the outer shape or the neighborhood
relations of the input dataset should be preserved in the created EAUM. Since the
ontology is still in development stage, only exemplary measures and parameters are
used so far, which will have to be adapted accordingly as research progresses.

[EAUMs](https://link.springer.com/article/10.1007/s42489-021-00072-5) are a special form
a [Cartogram](https://en.wikipedia.org/wiki/Cartogram).

The documentation can be found [here](DOCUMENTATION.md) and contains besides useful
references thoughts about the architecture of the ontology itself as well as about its
integration in e.g. SPARQL-Endpoints and possibilities for rule inclusion and queries.
